package main

import (
	"io/ioutil"
	"net/http"
	"net/http/httptest"
	"net/http/httputil"
	"strings"
	"testing"
)

func Benchmark_commHandler(b *testing.B) {
	b.ReportAllocs()
	w := httptest.NewRecorder()
	r := httptest.NewRequest("POST",
		"http://localhost:8080",
		strings.NewReader("{ \"Type\": \"Test.vpn\", \"Data\": \"Rabbit.log\" }"))

	for i := 0; i < b.N; i++ {
		commHandler(w, r)
	}
}

func Test_commHandler(t *testing.T) {
	type args struct {
		w http.ResponseWriter
		r *http.Request
	}
	tests := []struct {
		name    string
		args    args
		wantErr bool
	}{
		{
			"test rabbit log OK",
			args{
				httptest.NewRecorder(),
				httptest.NewRequest("POST",
					"http://localhost:8080",
					strings.NewReader("{ \"Type\": \"Test.vpn\", \"Data\": \"Rabbit.log\" }")),
			},
			false,
		},
		{
			"test Database processing OK",
			args{
				httptest.NewRecorder(),
				httptest.NewRequest("POST",
					"http://localhost:8080",
					strings.NewReader("{ \"Type\": \"Develop.mr_robot\", \"Data\": \"Database.processing\" }")),
			},
			false,
		},
		{
			"without type",
			args{
				httptest.NewRecorder(),
				httptest.NewRequest("POST",
					"http://localhost:8080",
					strings.NewReader("{\"Data\": \"Database.processing\" }")),
			},
			true,
		},
		{
			"without date",
			args{
				httptest.NewRecorder(),
				httptest.NewRequest("POST",
					"http://localhost:8080",
					strings.NewReader("{ \"Type\": \"Develop.mr_robot\" }")),
			},
			true,
		},
		{
			"test Database corrupt OK",
			args{
				httptest.NewRecorder(),
				httptest.NewRequest("POST",
					"http://localhost:8080",
					strings.NewReader("{ \"Type\": \"Develop.mr_robot\", \"Data\": \"Database.prddocessing\" }")),
			},
			true,
		},
		{
			"test Database wrong delim not ok",
			args{
				httptest.NewRecorder(),
				httptest.NewRequest("POST",
					"http://localhost:8080",
					strings.NewReader("{ \"Type\": \"Develop.mr_robot\", \"Data\": \"Database.processing\" }")),
			},
			true,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {

			//Dump request
			reqDump, _ := httputil.DumpRequest(tt.args.r, true)

			//Run function
			commHandler(tt.args.w, tt.args.r)

			//Cast responsewriter to response recorder
			tmp := tt.args.w.(*httptest.ResponseRecorder)
			if tmp.Code != 200 && !tt.wantErr {
				body, _ := ioutil.ReadAll(tmp.Result().Body)
				t.Errorf("validate() error = %s, request is %s ", body, reqDump)
			}
		})
	}
}
