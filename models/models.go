package models

//Наборы параметров
type Nabor struct {
	Id string `sql:",unique"`
}

//Типы параметров
type ParamsType struct {
	Id      string `sql:",unique"`
	NaborId string `sql:"on_delete:RESTRICT"`
	Nabor   *Nabor
	Params  map[string]string `sql:"type:hstore" pg:",hstore"`
}

//Тип параметра Processing
type Processing struct {
	Host     string `json:"host"`
	Port     string `json:"port"`
	Database string `json:"database"`
	User     string `json:"user"`
	Password string `json:"password"`
	Schema   string `json:"schema"`
}

//Тип параметра Log
type Log struct {
	Host        string `json:"host"`
	Port        string `json:"port"`
	Virtualhost string `json:"virtualhost"`
	User        string `json:"user"`
	Password    string `json:"password"`
}
