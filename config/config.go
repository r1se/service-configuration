package config

import (
	"encoding/json"
	"os"
)

// Application config
var Config *config

type config struct {
	Port     string `json:"port"`
	Database struct {
		Addr         string `json:"addr"`
		Username     string `json:"username"`
		Password     string `json:"password"`
		DatabaseName string `json:"database_name"`
	} `json:"database"`
	Typeregexp string `json:"typeregexp"`
	Dataregexp string `json:"dataregexp"`
	Datadelim string `json:"datadelim"`
	Debug string `json:"debug"`
}

func init() {
	Config = loadConfig()
}

// LoadConfig load the config.json file and return its data
func loadConfig() *config {
	appConfig := &config{}

	configFile, err := os.Open("config/config.json")
	defer configFile.Close()
	if err != nil {
		panic(err)
	}

	err = json.NewDecoder(configFile).Decode(&appConfig)
	if err != nil {
		panic(err)
	}

	return appConfig
}
