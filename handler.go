package main

import (
	"encoding/json"
	"github.com/go-pg/pg"
	"io/ioutil"
	"net/http"
	"service-configuration/config"
	"service-configuration/db"
	"service-configuration/models"
	"strings"
)

func commHandler(w http.ResponseWriter, r *http.Request) {
	data, err := ioutil.ReadAll(r.Body)
	if err != nil {
		http.Error(w, "cant make slice from body "+err.Error(), http.StatusBadRequest)
		return
	}

	request := struct {
		Type string `json:"Type"`
		Data string `json:"Data"`
	}{}
	err = json.Unmarshal(data, &request)
	if err != nil {
		http.Error(w, "cant request slice "+err.Error(), http.StatusBadRequest)
		return
	}

	if !typereg.MatchString(request.Type) {
		http.Error(w, "Type field is NOT valid.", http.StatusBadRequest)
		return
	}

	if !datareg.MatchString(request.Data) {
		http.Error(w, "Data field is NOT valid.", http.StatusBadRequest)
		return
	}

	sliceNabor := strings.Split(request.Data, config.Config.Datadelim)
	if len(sliceNabor) < 2 {
		http.Error(w, "Data slice is NOT valid.", http.StatusBadRequest)
		return
	}

	stmt, err := db.Conn.Prepare(`SELECT params -> $1::text FROM params_types WHERE nabor_id=$2::text AND id=$3::text`)
	if err != nil {
		http.Error(w, "SQL statement prepare error. "+err.Error(), http.StatusBadRequest)
		return
	}
	defer stmt.Close()

	var res string
	_, err = stmt.Query(pg.Scan(&res), request.Type, sliceNabor[0], sliceNabor[1])
	if err != nil {
		http.Error(w, "query error. "+err.Error(), http.StatusBadRequest)
		return
	}
	if res == "" {
		http.Error(w, "{}", http.StatusBadRequest)
		return
	}

	b := []byte(res)
	//ParamType switch
	switch sliceNabor[1] {
	case "processing":
		err = json.Unmarshal(b, &models.Processing{})
		if err != nil {
			http.Error(w, "cant unmarshal answer"+err.Error(), http.StatusBadRequest)
			return
		}
		break

	case "log":
		err = json.Unmarshal(b, &models.Log{})
		if err != nil {
			http.Error(w, "cant unmarshal answer"+err.Error(), http.StatusBadRequest)
			return
		}
		break

	default:
		http.Error(w, "{}"+err.Error(), http.StatusBadRequest)
		return
	}

	w.Header().Set("Content-Type", "text/plain")
	w.WriteHeader(200)
	w.Write(b)
	return
}
