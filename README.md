Зависимости:  
go get github.com/go-pg/pg  
go get github.com/go-pg/migrations  

По структуре:  
main - стартует http server которые обабатывает запросы  
handler - валидирует запрос через regexp, забирает информацию из базы, приводит к необходиму json  
config - загрузка и валидация конфига из файла  
models - описание структуры дб, наборов и параметров, и структуры json на возвращение  
migrations - создание таблиц и индексов, посев тестовых данных  

Я решил использовать go-pg/migrations для управлениями миграция.  

Таблица gopg_migrations сохраняет информацию по всем выполненным миграциям.  

Для запуска всех миграций:  
go run db/migrations/!(*_test).go up up  

Для того чтобы откатить последнию миграцию:  
go run db/migrations/!(*_test).go up down  

Для того чтобы скинуть все миграции:  
go run db/migrations/!(*_test).go upreset  

Для того чтобы узнать версию текущей миграции:  
go run db/migrations/!(*_test).go up version  

Для того чтобы сменить версию миграции:  
go run db/migrations/!(*_test).go up set_version %version number%  