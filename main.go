package main

import (
	"errors"
	"fmt"
	"github.com/go-pg/pg"
	"log"
	"net/http"
	"regexp"
	"service-configuration/config"
	"service-configuration/db"
	"time"
)

var (
	typereg *regexp.Regexp
	datareg *regexp.Regexp
)

func init() {
	typereg = regexp.MustCompile(config.Config.Typeregexp)
	datareg = regexp.MustCompile(config.Config.Dataregexp)
}

//Wrap some panics, because http must run forever
func RecoverWrap(h http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		var err error
		defer func() {
			r := recover()
			if r != nil {
				switch t := r.(type) {
				case string:
					err = errors.New(t)
				case error:
					err = t
				default:
					err = errors.New("Unknown error")
				}
				log.Println(err.Error())
				http.Error(w, err.Error(), http.StatusInternalServerError)
			}
		}()
		h.ServeHTTP(w, r)
	})
}

func main() {
	// Set debug
	if config.Config.Debug == "true" {
		db.Conn.OnQueryProcessed(func(event *pg.QueryProcessedEvent) {
			query, err := event.FormattedQuery()
			if err != nil {
				panic(err)
			}
			log.Printf("%s %s", time.Since(event.StartTime), query)
		})
	}

	//config is global from config package
	fmt.Printf("Server listening on port %s", config.Config.Port)
	http.Handle("/", RecoverWrap(http.HandlerFunc(commHandler)))
	srv := &http.Server{
		Addr:         ":" + config.Config.Port,
		ReadTimeout:  5 * time.Second,
		WriteTimeout: 10 * time.Second,
	}
	log.Fatal(srv.ListenAndServe())
}
