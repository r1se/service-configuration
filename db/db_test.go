package db

import (
	"testing"

	"github.com/go-pg/pg"
	"service-configuration/config"
)

func Test_connect(t *testing.T) {
	Connecting:= pg.Connect(&pg.Options{
		Addr:     config.Config.Database.Addr,
		User:     config.Config.Database.Username,
		Password: config.Config.Database.Password,
		Database: config.Config.Database.DatabaseName,
	})
	tests := []struct {
		name string
		want *pg.DB
	}{

	{"got DB postgresql",
		Connecting,
	},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			got := connect()
			if(got.Options().Addr!= tt.want.Options().Addr) {
				t.Errorf("connect() = %v, want %v", got, tt.want)
			}
		})
	}
}
