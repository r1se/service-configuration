package main

import (
	"flag"
	"fmt"
	"github.com/go-pg/migrations"
	"github.com/go-pg/pg"
	"log"
	"os"
	"service-configuration/config"
	"service-configuration/db"
	"time"
)

const usageText = `This program runs command on the db. Supported commands are:
	- init - creates gopg_migrations table.
	- up - runs all available migrations.
	- down - reverts last migration.
	- reset - reverts all migrations.
	- version - prints current db version.
	- set_version [version] - sets db version without running migrations.
	go run *.go <command> [args]
`

func main() {
	flag.Usage = usage
	flag.Parse()

	if config.Config.Debug == "true" {
		db.Conn.OnQueryProcessed(func(event *pg.QueryProcessedEvent) {
			query, err := event.FormattedQuery()
			if err != nil {
				panic(err)
			}

			log.Printf("%s %s", time.Since(event.StartTime), query)
		})
	}

	oldVersion, newVersion, err := migrations.Run(db.Conn, flag.Args()...)
	if err != nil {
		exitf(err.Error())
	}
	if newVersion != oldVersion {
		fmt.Printf("migrated from version %d to %d\n", oldVersion, newVersion)
	} else {
		fmt.Printf("version is %d\n", oldVersion)
	}
}

func usage() {
	fmt.Printf(usageText)
	flag.PrintDefaults()
	os.Exit(2)
}

func errorf(s string, args ...interface{}) {
	fmt.Fprintf(os.Stderr, s+"\n", args...)
}

func exitf(s string, args ...interface{}) {
	errorf(s, args...)
	os.Exit(1)
}
