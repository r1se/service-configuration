package main

import (
	"testing"

	"github.com/go-pg/migrations"
	"github.com/go-pg/pg"
	"service-configuration/config"
)

func Test_updata(t *testing.T) {

	Connecting:= pg.Connect(&pg.Options{
		Addr:     config.Config.Database.Addr,
		User:     config.Config.Database.Username,
		Password: config.Config.Database.Password,
		Database: config.Config.Database.DatabaseName,
	})

	type args struct {
		db migrations.DB
	}
	tests := []struct {
		name    string
		args    args
		wantErr bool
	}{
		{
			"Test update data up",
			args{Connecting},
			false,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if err := updata(tt.args.db); (err != nil) != tt.wantErr {
				t.Errorf("updata() error = %v, wantErr %v", err, tt.wantErr)
			}
		})
	}
}

func Test_downdata(t *testing.T) {
	Connecting:= pg.Connect(&pg.Options{
		Addr:     config.Config.Database.Addr,
		User:     config.Config.Database.Username,
		Password: config.Config.Database.Password,
		Database: config.Config.Database.DatabaseName,
	})
	type args struct {
		db migrations.DB
	}
	tests := []struct {
		name    string
		args    args
		wantErr bool
	}{
		{
			"Test update data down",
			args{Connecting},
			false,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if err := downdata(tt.args.db); (err != nil) != tt.wantErr {
				t.Errorf("downdata() error = %v, wantErr %v", err, tt.wantErr)
			}
		})
	}
}
