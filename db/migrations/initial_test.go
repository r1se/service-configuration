package main

import (
	"testing"

	"github.com/go-pg/migrations"
	"github.com/go-pg/pg"
	"service-configuration/config"
)

func Test_up(t *testing.T) {

	Connecting:= pg.Connect(&pg.Options{
		Addr:     config.Config.Database.Addr,
		User:     config.Config.Database.Username,
		Password: config.Config.Database.Password,
		Database: config.Config.Database.DatabaseName,
	})

	type args struct {
		db migrations.DB
	}
	tests := []struct {
		name    string
		args    args
		wantErr bool
	}{
	{
		"Test db create",
		args{Connecting},
		false,
	},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if err := up(tt.args.db); (err != nil) != tt.wantErr {
				t.Errorf("up() error = %v, wantErr %v", err, tt.wantErr)
			}
		})
	}
}

func Test_down(t *testing.T) {

	Connecting:= pg.Connect(&pg.Options{
		Addr:     config.Config.Database.Addr,
		User:     config.Config.Database.Username,
		Password: config.Config.Database.Password,
		Database: config.Config.Database.DatabaseName,
	})

	type args struct {
		db migrations.DB
	}
	tests := []struct {
		name    string
		args    args
		wantErr bool
	}{
		{
			"Test db down",
			args{Connecting},
			false,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if err := down(tt.args.db); (err != nil) != tt.wantErr {
				t.Errorf("down() error = %v, wantErr %v", err, tt.wantErr)
			}
		})
	}
}
