package main

import (
	"encoding/json"
	"github.com/go-pg/migrations"
	"service-configuration/models"
)

func updata(db migrations.DB) error {

	//Создаем тип наборов
	_, err := db.Model(&models.Nabor{"Database"}, &models.Nabor{"Rabbit"}).OnConflict("DO NOTHING").Insert()
	if err != nil {
		panic(err)
		return err
	}

	//Создаем и заполняем типы параметров
	out, err := json.Marshal(&models.Processing{"localhost",
		"5432",
		"devdb",
		"mr_robot",
		"secret",
		"public"})
	if err != nil {
		panic(err)
		return err
	}

	_, err = db.Model(&models.ParamsType{Id: "processing", NaborId: "Database", Params: map[string]string{
		"Develop.mr_robot": string(out[:])}}).OnConflict("DO NOTHING").Insert()
	if err != nil {
		panic(err)
		return err
	}

	out, _ = json.Marshal(&models.Log{"10.0.5.42",
		"5671",
		"/",
		"guest",
		"guest"})
	if err != nil {
		panic(err)
		return err
	}
	_, err = db.Model(&models.ParamsType{Id: "log", NaborId: "Rabbit", Params: map[string]string{
		"Test.vpn": string(out[:])}}).OnConflict("DO NOTHING").Insert()
	if err != nil {
		panic(err)
		return err
	}

	return nil
}

func downdata(db migrations.DB) error {

	//Удаляем данные из ParamsType
	_, err := db.Model(&models.ParamsType{}).Where("id IN (?, ?)", "processing", "log").Delete()
	if err != nil {
		panic(err)
		return err
	}

	//Удаляем данные из Nabor
	_, err = db.Model(&models.Nabor{}).Where("id IN (?, ?)", "Database", "Rabbit").Delete()
	if err != nil {
		panic(err)
		return err
	}

	return nil
}

func init() {
	migrations.Register(updata, downdata)
}
