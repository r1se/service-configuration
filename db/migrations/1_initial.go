package main

import (
	"github.com/go-pg/migrations"
	"github.com/go-pg/pg/orm"
	"service-configuration/models"
)

func up(db migrations.DB) error {

	for _, model := range []interface{}{&models.Nabor{}, &models.ParamsType{}} {
		err := db.CreateTable(model, &orm.CreateTableOptions{
			IfNotExists: true,
			FKConstraints: true,
		})

		if err != nil {
			panic(err)
			return err
		}
	}
	db.Exec("CREATE INDEX hidx ON params_types USING GIN (params);	")
	return nil
}

func down(db migrations.DB) error {

	for _, model := range []interface{}{&models.Nabor{}, &models.ParamsType{}} {
		err := db.DropTable(model, &orm.DropTableOptions{
			IfExists: true,
			Cascade:  true,
		})

		if err != nil {
			panic(err)
			return err
		}
	}
	return nil
}

func init() {
	migrations.Register(up, down)
}
